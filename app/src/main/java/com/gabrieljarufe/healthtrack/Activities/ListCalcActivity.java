package com.gabrieljarufe.healthtrack.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gabrieljarufe.healthtrack.Models.CalculatorItem;
import com.gabrieljarufe.healthtrack.Models.OnItemClickListener;
import com.gabrieljarufe.healthtrack.Models.Register;
import com.gabrieljarufe.healthtrack.R;
import com.gabrieljarufe.healthtrack.SqlHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ListCalcActivity extends AppCompatActivity {

    private RecyclerView rvListIMC;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_calc);
        rvListIMC = findViewById(R.id.recycler_view_list);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String type = extras.getString("type");
            new Thread(() -> {
                List<Register> registers = SqlHelper.getInstance(this).getRegisterBy(type);
                runOnUiThread(() -> {
                    Log.i("Teste", registers.toString());
                    rvListIMC.setLayoutManager(new LinearLayoutManager(this));
                    ListCalcAdapter adapter = new ListCalcAdapter(registers);
                    rvListIMC.setAdapter(adapter);
                });
            }).start();
        }
    }

    private class ListCalcAdapter extends RecyclerView.Adapter<ListCalcViewHolder> {

        private final List<Register> datas;

        public ListCalcAdapter(List<Register> datas) {
            this.datas = datas;
        }

        @NonNull
        public ListCalcViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new ListCalcViewHolder(getLayoutInflater().inflate(R.layout.data_list_imc, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull ListCalcViewHolder holder, int position) {
            Register data = datas.get(position);

            // listener para ouvir evento de click e de long-click (segurar touch)
            holder.bind(data);
        }

        @Override
        public int getItemCount() {
            return datas.size();
        }

    }

    private class ListCalcViewHolder extends RecyclerView.ViewHolder {

        ListCalcViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        void bind(Register data) {
            String formatted = "";
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("pt", "BR"));
                Date dateSaved = sdf.parse(data.createdDate);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("pt", "BR"));
                formatted = dateFormat.format(dateSaved);
            } catch (ParseException e) {
            }

            TextView imc_date = itemView.findViewById(R.id.imc_date);
            TextView imc_res = itemView.findViewById(R.id.imc_res);
            imc_date.setText(getString(R.string.list_data, formatted));
            imc_res.setText(getString(R.string.list_res, data.response));
        }

    }

}