package com.gabrieljarufe.healthtrack.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.gabrieljarufe.healthtrack.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TmbActivity extends AppCompatActivity {
    private EditText edt_tmb_weight;
    private EditText edt_tmb_height;
    private EditText edt_tmb_age;
    private Button btn_tmb_calculate;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tmb);
        edt_tmb_weight = findViewById(R.id.edt_tmb_weight);
        edt_tmb_height = findViewById(R.id.edt_tmb_height);
        edt_tmb_age = findViewById(R.id.edt_tmb_age);
        btn_tmb_calculate = findViewById(R.id.btn_tmb_calculate);
        spinner = findViewById(R.id.spn_tmb_lifestyle);

        List<String> lines = Arrays.asList(getResources().getStringArray(R.array.tmb_lifestyle));
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, R.layout.style_spinner, lines);
        spinner.setAdapter(arrayAdapter);

        btn_tmb_calculate.setOnClickListener(view -> {
            if (!validate()) {
                Toast.makeText(TmbActivity.this, R.string.field_messages_errors, Toast.LENGTH_LONG).show();
                return;
            }
            String sHeight = edt_tmb_height.getText().toString();
            String sWeight = edt_tmb_weight.getText().toString();
            String sAge = edt_tmb_age.getText().toString();
            int height = Integer.parseInt(sHeight);
            int weight = Integer.parseInt(sWeight);
            int age = Integer.parseInt(sAge);
            AlertDialog alertDialog = new AlertDialog.Builder(TmbActivity.this)
                    .setTitle(getString(R.string.tmb_response))
                    .setMessage((Integer.toString(calculateTMB(weight, height, age))) + " calorias!")
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    })
                    .create();
            alertDialog.show();

            InputMethodManager inm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inm.hideSoftInputFromWindow(edt_tmb_weight.getWindowToken(), 0);
            inm.hideSoftInputFromWindow(edt_tmb_height.getWindowToken(), 0);
            inm.hideSoftInputFromWindow(edt_tmb_age.getWindowToken(), 0);
        });

    }

    private boolean validate() {
        return !edt_tmb_height.getText().toString().startsWith("0")
                && !edt_tmb_height.getText().toString().isEmpty()
                && !edt_tmb_weight.getText().toString().startsWith("0")
                && !edt_tmb_weight.getText().toString().isEmpty()
                && !edt_tmb_age.getText().toString().isEmpty();
    }

    private int calculateTMB(int weight, int height, int age) {
        return (int) (66 + (13.8 * weight) + (5.0 * height) - (4.7 * age));
    }
}