package com.gabrieljarufe.healthtrack.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gabrieljarufe.healthtrack.R;
import com.gabrieljarufe.healthtrack.SqlHelper;

public class ImcActivity extends AppCompatActivity {

    private EditText edt_imc_weight;
    private EditText edt_imc_height;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);
        edt_imc_weight = findViewById(R.id.edt_imc_weight);
        edt_imc_height = findViewById(R.id.edt_imc_height);
        Button btn_calculate = findViewById(R.id.btn_calculate_imc);

        btn_calculate.setOnClickListener(view -> {
            if (!validate()) {
                Toast.makeText(getApplicationContext(), R.string.field_messages_errors, Toast.LENGTH_LONG).show();
                return;
            }
            String sHeight = edt_imc_height.getText().toString();
            String sWeight = edt_imc_weight.getText().toString();
            int height = Integer.parseInt(sHeight);
            int weight = Integer.parseInt(sWeight);
            double resultBMI = calculateBMI(height, weight);
            int bmiResponseId = bmiResponse(resultBMI);
            AlertDialog alertDialog = new AlertDialog.Builder(ImcActivity.this)
                    .setTitle(getString(R.string.imc_response, resultBMI))
                    .setMessage(bmiResponseId)
                    .setPositiveButton(android.R.string.ok, (dialog, which) -> {
                    })
                    .setNegativeButton(R.string.save, ((dialog, which) -> {
                        new Thread(() -> {
                            long calcId = SqlHelper.getInstance(ImcActivity.this).addItem("IMC", resultBMI);
                            runOnUiThread(() -> {
                                if (calcId > 0) {
                                    Toast.makeText(ImcActivity.this, R.string.saved, Toast.LENGTH_SHORT).show();
                                    openListCalcActivity();
                                }
                            });
                        }).start();

                    }))
                    .create();
            alertDialog.show();

            InputMethodManager inm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inm.hideSoftInputFromWindow(edt_imc_weight.getWindowToken(), 0);
            inm.hideSoftInputFromWindow(edt_imc_height.getWindowToken(), 0);
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.menu_list){
            openListCalcActivity();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return true;
    }

    private void openListCalcActivity() {
        Intent intent = new Intent(ImcActivity.this, ListCalcActivity.class);
        intent.putExtra("type", "IMC");
        startActivity(intent);
    }

    @StringRes
    private int bmiResponse(double bmi) {
        if (bmi < 15)
            return R.string.imc_severely_low_weight;
        else if (bmi < 16)
            return R.string.imc_very_low_weight;
        else if (bmi < 18.5)
            return R.string.imc_low_weight;
        else if (bmi < 25)
            return R.string.normal;
        else if (bmi < 30)
            return R.string.imc_high_weight;
        else if (bmi < 35)
            return R.string.imc_so_high_weight;
        else if (bmi < 40)
            return R.string.imc_severely_high_weight;
        else
            return R.string.imc_extreme_weight;
    }

    private double calculateBMI(int height, int weight) {
        return weight / (Math.pow((double) height / 100, 2));
    }

    private boolean validate() {
        return !edt_imc_height.getText().toString().startsWith("0")
                && !edt_imc_height.getText().toString().isEmpty()
                && !edt_imc_weight.getText().toString().startsWith("0")
                && !edt_imc_weight.getText().toString().isEmpty();
    }
}