package com.gabrieljarufe.healthtrack.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gabrieljarufe.healthtrack.Models.CalculatorItem;
import com.gabrieljarufe.healthtrack.Models.OnItemClickListener;
import com.gabrieljarufe.healthtrack.R;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvMain;
    private CardView searchbar;
    private boolean searchBarBoolean = false;
    private EditText edt_search_bar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        searchbar = findViewById(R.id.search_bar);
        rvMain = findViewById(R.id.rv_main);
        edt_search_bar = findViewById(R.id.edt_search_bar);
        List<CalculatorItem> calculatorItems = new ArrayList<>();
        calculatorItems.add(new CalculatorItem(1, R.drawable.ic_baseline_wb_sunny_24, R.string.label_imc));
        calculatorItems.add(new CalculatorItem(2, R.drawable.ic_baseline_accessibility_24, R.string.label_tmb));
        // Definir o comportamento de exibição
        // mosaic
        //grid
        //linear (horizontal || vertical)
        searchbar.setOnClickListener(view -> {
            if (!searchBarBoolean) {
                searchbar.setCardElevation(20);
            }
        });
        rvMain.setLayoutManager(new LinearLayoutManager(this));
        MainAdapter adapter = new MainAdapter(calculatorItems);
        rvMain.setAdapter(adapter);
        adapter.setListener(id -> {
            switch (id) {
                case 1:
                    startActivity(new Intent(MainActivity.this, ImcActivity.class));
                    break;
                case 2:
                    startActivity(new Intent(MainActivity.this, TmbActivity.class));
            }

        });
        //btn_imc = findViewById(R.id.btn_imc);

    }

    private class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {

        private List<CalculatorItem> calculatorItems;
        private OnItemClickListener listener;

        public MainAdapter(List<CalculatorItem> calculatorItems) {
            this.calculatorItems = calculatorItems;
        }

        public void setListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @NonNull
        @Override
        public MainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new MainViewHolder(getLayoutInflater().inflate(R.layout.calculator_item, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull MainViewHolder holder, int position) {
            CalculatorItem calculatorItemsCurrent = calculatorItems.get(position);
            holder.bind(calculatorItemsCurrent);
        }

        @Override
        public int getItemCount() {
            return calculatorItems.size();
        }

        private class MainViewHolder extends RecyclerView.ViewHolder {

            public MainViewHolder(@NonNull View itemView) {
                super(itemView);
            }

            public void bind(CalculatorItem item) {
                TextView textView = itemView.findViewById(R.id.txt_item_name);
                ImageView imageView = itemView.findViewById(R.id.img_item_icon);
                LinearLayout container = (LinearLayout) itemView;

                textView.setText(item.getTextStringId());
                imageView.setImageResource(item.getDrawable());

                container.setOnClickListener(view -> {
                    listener.onClick(item.getId());
                });
            }
        }
    }
}
