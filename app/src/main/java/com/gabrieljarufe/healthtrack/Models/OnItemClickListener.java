package com.gabrieljarufe.healthtrack.Models;

public interface OnItemClickListener {
    void onClick(int id);
}
