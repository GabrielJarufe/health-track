package com.gabrieljarufe.healthtrack.Models;

public class CalculatorItem {
    private int id;
    private int drawable;
    private int textStringId;

    public CalculatorItem(int id, int drawable, int textStringId) {
        this.id = id;
        this.drawable = drawable;
        this.textStringId = textStringId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public int getTextStringId() {
        return textStringId;
    }

    public void setTextStringId(int textStringId) {
        this.textStringId = textStringId;
    }
}
